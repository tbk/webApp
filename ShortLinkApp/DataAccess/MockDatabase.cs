﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessObject;

namespace DataAccess
{
    //AppUser
    public partial class MockDatabase : IDatabase
    {
        public static bool AddAppUserInDbThrow { get; set; }

        public static bool IsAppUserInDbReturn { get; set; }
        public static bool IsEmailInDbReturn { get; set; }

        public static AppUser GetAppUserFromDbReturn { get; set; }


        public void AddAppUserInDb(AppUser appUser)
        {
            if (AddAppUserInDbThrow)
            {
                throw new Exception("AddAppUserInDb called");
            }
        }


        public bool IsAppUserInDb(int id)
        {
            return IsAppUserInDbReturn;
        }

        public bool IsEmailInDb(string email)
        {
            return IsEmailInDbReturn;
        }


        public AppUser GetAppUserFromDb(string email)
        {
            return GetAppUserFromDbReturn;
        }

        public AppUser GetAppUserFromDb(string email, string password)
        {
            return GetAppUserFromDbReturn;
        }
    }

    //TempUser
    public partial class MockDatabase : IDatabase
    {
        public static bool AddTempUserInDbThrow { get; set; }

        public static bool IsTempUserInDbReturn { get; set; }

        public static TempUser GetTempUserFromDbReturn { get; set; }
        public static List<TempUser> GetAllTempUsersFromDbReturn { get; set; }

        public static bool DeleteTempUserFromDbThrow { get; set; }


        public void AddTempUserInDb(TempUser tempUser)
        {
            if (AddTempUserInDbThrow)
            {
                throw new Exception("AddTempUserInDb called");
            }
        }


        public bool IsTempUserInDb(string tempUserKey)
        {
            return IsTempUserInDbReturn;
        }

        public bool IsTempUserInDb(int id)
        {
            return IsTempUserInDbReturn;
        }


        public TempUser GetTempUserFromDb(string userKey)
        {
            return GetTempUserFromDbReturn;
        }

        public List<TempUser> GetAllTempUsersFromDb()
        {
            return GetAllTempUsersFromDbReturn;
        }


        public void DeleteTempUserFromDb(string userKey)
        {
            if (DeleteTempUserFromDbThrow)
            {
                throw new Exception("DeleteTempUserFromDb called");
            }
        }
    }

    //AppUserStatus
    public partial class MockDatabase : IDatabase
    {
        public static bool AddAppUserStatusInDbThrow { get; set; }

        public static bool IsAppUserStatusInDbReturn { get; set; }

        public static AppUserStatus GetAppUserStatusFromDbReturn { get; set; }


        public void AddAppUserStatusInDb(AppUserStatus userStatus)
        {
            if (AddAppUserStatusInDbThrow)
            {
                throw new Exception("AddAppUserStatusInDb called");
            }
        }


        public bool IsAppUserStatusInDb(string status)
        {
            return IsAppUserStatusInDbReturn;
        }

        public bool IsAppUserStatusInDb(int id)
        {
            return IsAppUserStatusInDbReturn;
        }


        public AppUserStatus GetAppUserStatusFromDb(string status)
        {
            return GetAppUserStatusFromDbReturn;
        }
    }

    //Link
    public partial class MockDatabase : IDatabase
    {
        public static bool AddLinkInDbThrow { get; set; }

        public static bool IsShortUrlInDbReturn { get; set; }

        public static bool UpdateLinkInDbThrow { get; set; }

        public static Link GetLinkFromDbReturn { get; set; }
        public static List<Link> GetAppUserLinksFromDbReturn { get; set; }
        public static List<Link> GetTempUserLinksFromDbReturn { get; set; }


        public void AddLinkInDb(Link link)
        {
            if (AddLinkInDbThrow)
            {
                throw new Exception("AddLinkInDb called");
            }
        }


        public bool IsShortUrlInDb(string shortUrl)
        {
            return IsShortUrlInDbReturn;
        }

        public bool IsShortUrlInDb(int id)
        {
            return IsShortUrlInDbReturn;
        }


        public void UpdateLinkInDb(string shortUrl)
        {
            if (UpdateLinkInDbThrow)
            {
                throw new Exception("UpdateLinkInDb called");
            }
        }


        public Link GetLinkFromDb(string shortUrl)
        {
            return GetLinkFromDbReturn;
        }

        public List<Link> GetAppUserLinksFromDb(string userEmail)
        {
            return GetAppUserLinksFromDbReturn;
        }

        public List<Link> GetTempUserLinksFromDb(string userKey)
        {
            return GetTempUserLinksFromDbReturn;
        }
    }

    //LinkControllers
    public partial class MockDatabase : IDatabase
    {
        public static bool AddLinkTempUserControllerInDbThrow { get; set; }
        public static bool AddLinkAppUserControllerInDbThrow { get; set; }

        public static bool IsLinkTempUserControllerInDbReturn { get; set; }

        public static LinkAppUserController GetLinkAppUserControllerFromDbReturn { get; set; }
        public static LinkTempUserController GetLinkTempUserControllerFromDbReturn { get; set; }
        public static List<LinkTempUserController> GetLinkTempUserControllersFromDbReturn { get; set; }
        public static List<LinkTempUserController> GetAllLinkTempUserControllerFromDbReturn { get; set; }

        public static bool DeleteLinkControllerFromDbThrow { get; set; }
        public static bool DeleteLinkTempUserControllerFromDbThrow { get; set; }

        public static bool MoveTempLinksToUserThrow { get; set; }


        public void AddLinkTempUserControllerInDb(Link link, TempUser tempUser)
        {
            if (AddLinkTempUserControllerInDbThrow)
            {
                throw new Exception("AddLinkTempUserControllerInDb called");
            }
        }

        public void AddLinkAppUserControllerInDb(Link link, AppUser appUser)
        {
            if (AddLinkAppUserControllerInDbThrow)
            {
                throw new Exception("AddLinkAppUserControllerInDb called");
            }
        }

        public bool IsLinkTempUserControllerInDb(int id)
        {
            return IsLinkTempUserControllerInDbReturn;
        }


        public LinkAppUserController GetLinkAppUserControllerFromDb(Link link)
        {
            return GetLinkAppUserControllerFromDbReturn;
        }

        public LinkTempUserController GetLinkTempUserControllerFromDb(Link link)
        {
            return GetLinkTempUserControllerFromDbReturn;
        }

        public LinkTempUserController GetLinkTempUserControllerFromDb(int id)
        {
            return GetLinkTempUserControllerFromDbReturn;
        }

        public List<LinkTempUserController> GetLinkTempUserControllersFromDb(string userKey)
        {
            return GetLinkTempUserControllersFromDbReturn;
        }

        public List<LinkTempUserController> GetAllLinkTempUserControllerFromDb()
        {
            return GetAllLinkTempUserControllerFromDbReturn;
        }


        public void DeleteLinkControllerFromDb(string shortUrl)
        {
            if (DeleteLinkControllerFromDbThrow)
            {
                throw new Exception("DeleteLinkControllerFromDb called");
            }
        }

        public void DeleteLinkTempUserControllerFromDb(int id)
        {
            if (DeleteLinkTempUserControllerFromDbThrow)
            {
                throw new Exception("DeleteLinkTempUserControllerFromDb called");
            }
        }


        public void MoveTempLinksToUser(string tempUserKey, string appUserEmail)
        {
            if (MoveTempLinksToUserThrow)
            {
                throw new Exception("MoveTempLinksToUser called");
            }
        }
    }
}
