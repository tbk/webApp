﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessObject;

namespace DataAccess
{
    public partial class Database
    {
        private AppDbEntities entity;

        public Database(AppDbEntities _entity = null)
        {
            if (_entity == null)
            {
                entity = new AppDbEntities();
            }
            else
            {
                entity = _entity;
            }
        }
    }

    //AppUser
    public partial class Database : IDatabase
    {
        public void AddAppUserInDb(AppUser appUser)
        {
            if (IsEmailInDb(appUser.Email))
            {
                throw new ArgumentException("Email is already in Db");
            }
            if (!IsAppUserStatusInDb(appUser.AppUserStatusId))
            {
                throw new ArgumentException("Invalid appUserId");
            }
            entity.AppUsers.Add(appUser);
            entity.SaveChanges();
        }


        public bool IsAppUserInDb(int id)
        {
            if (entity.AppUsers.Where(x => x.Id == id).FirstOrDefault() != null)
            {
                return true;
            }
            return false;
        }

        public bool IsEmailInDb(string email)
        {
            if (entity.AppUsers.Where(x => x.Email.ToLower().Equals(email.ToLower())).FirstOrDefault() != null)
            {
                return true;
            }
            return false;
        }


        public AppUser GetAppUserFromDb(string email)
        {
            if (!IsEmailInDb(email))
            {
                throw new ArgumentException("No such AppUser in Db");
            }
            return entity.AppUsers.Where(x => x.Email.ToLower().Equals(email.ToLower())).FirstOrDefault();
        }

        public AppUser GetAppUserFromDb(string email, string password)
        {
            AppUser user = new AppUser();
            user = entity.AppUsers.Where(x => x.Email.ToLower().Equals(email.ToLower())
                                        && x.Password.Equals(password)).FirstOrDefault();
            if (user != null)
            {
                return user;
            }
            return null;
        }
    }

    //TempUser
    public partial class Database : IDatabase
    {
        public void AddTempUserInDb(TempUser tempUser)
        {
            if (IsTempUserInDb(tempUser.Key))
            {
                throw new ArgumentException("Temp user is already in Db");
            }
            entity.TempUsers.Add(tempUser);
            entity.SaveChanges();
        }


        public bool IsTempUserInDb(string tempUserKey)
        {
            if (entity.TempUsers.Where(x => x.Key.Equals(tempUserKey)).FirstOrDefault() != null)
            {
                return true;
            }
            return false;
        }

        public bool IsTempUserInDb(int id)
        {
            if (entity.TempUsers.Where(x => x.Id == id).FirstOrDefault() != null)
            {
                return true;
            }
            return false;
        }


        public TempUser GetTempUserFromDb(string userKey)
        {
            if (!IsTempUserInDb(userKey))
            {
                throw new ArgumentException("No such tempUser in Db");
            }
            return entity.TempUsers.Where(x => x.Key.Equals(userKey)).FirstOrDefault();
        }

        public List<TempUser> GetAllTempUsersFromDb()
        {
            return entity.TempUsers.ToList();
        }


        public void DeleteTempUserFromDb(string userKey)
        {
            if (!IsTempUserInDb(userKey))
            {
                throw new ArgumentException("No userKey in Db.");
            }
            TempUser tempUser = GetTempUserFromDb(userKey);
            entity.TempUsers.Remove(tempUser);
            entity.SaveChanges();
        }
    }

    //AppUserStatus
    public partial class Database : IDatabase
    {
        public void AddAppUserStatusInDb(AppUserStatus userStatus)
        {
            if (IsAppUserStatusInDb(userStatus.Status))
            {
                throw new ArgumentException("Status is already in Db");
            }
            entity.AppUserStatuses.Add(userStatus);
            entity.SaveChanges();
        }


        public bool IsAppUserStatusInDb(string status)
        {
            if (entity.AppUserStatuses.Where(x => x.Status.Equals(status)).FirstOrDefault() != null)
            {
                return true;
            }
            return false;
        }
        
        public bool IsAppUserStatusInDb(int id)
        {
            if (entity.AppUserStatuses.Where(x => x.Id == id).FirstOrDefault() != null)
            {
                return true;
            }
            return false;
        }


        public AppUserStatus GetAppUserStatusFromDb(string status)
        {
            if (!IsAppUserStatusInDb(status))
            {
                throw new ArgumentException("No such AppUserStatus in Db");
            }
            return entity.AppUserStatuses.Where(x => x.Status.Equals(status)).FirstOrDefault();
        }
    }

    //Link
    public partial class Database : IDatabase
    {
        public void AddLinkInDb(Link link)
        {
            if (IsShortUrlInDb(link.ShortUrl))
            {
                throw new ArgumentException("Link is already in Db");
            }
            entity.Links.Add(link);
            entity.SaveChanges();
        }


        public bool IsShortUrlInDb(int id)
        {
            if (entity.Links.Where(x => x.Id == id).FirstOrDefault() != null)
            {
                return true;
            }
            return false;
        }

        public bool IsShortUrlInDb(string shortUrl)
        {
            if (entity.Links.Where(x => x.ShortUrl.Equals(shortUrl)).FirstOrDefault() != null)
            {
                return true;
            }
            return false;
        }


        public void UpdateLinkInDb(string shortUrl)
        {
            Link link = GetLinkFromDb(shortUrl);

            link.ClickCount++;
            link.LastVisit = DateTime.Now;
            entity.SaveChanges();
        }


        public Link GetLinkFromDb(string shortUrl)
        {
            if (!IsShortUrlInDb(shortUrl))
            {
                throw new ArgumentException("No such shortUrl in Db");
            }
            return entity.Links.Where(x => x.ShortUrl.Equals(shortUrl)).FirstOrDefault();
        }

        public List<Link> GetAppUserLinksFromDb(string userEmail)
        {
            if (!IsEmailInDb(userEmail))
            {
                throw new ArgumentException("No such appUser in Db");
            }
            AppUser appUser = GetAppUserFromDb(userEmail);
            List<Link> linkList = new List<Link>();
            List<LinkAppUserController> linkApppUserList = entity.LinksAppUserController.Where(x => x.AppUserId == appUser.Id).ToList();
            foreach (var item in linkApppUserList)
            {
                linkList.Add(entity.Links.Where(x => x.Id == item.LinkId).FirstOrDefault());
            }
            return linkList;
        }

        public List<Link> GetTempUserLinksFromDb(string userKey)
        {
            if (!IsTempUserInDb(userKey))
            {
                throw new ArgumentException("No such tempUser in Db");
            }

            TempUser tempUser = GetTempUserFromDb(userKey);
            List<Link> linkList = new List<Link>();
            List<LinkTempUserController> linkTempUserList = entity.LinksTempUserController.Where(x => x.TempUserId == tempUser.Id).ToList();
            foreach (var item in linkTempUserList)
            {
                linkList.Add(entity.Links.Where(x => x.Id == item.LinkId).FirstOrDefault());
            }
            return linkList;
        }
    }

    //LinkControllers
    public partial class Database : IDatabase
    {
        public void AddLinkTempUserControllerInDb(Link link, TempUser tempUser)
        {
            LinkTempUserController linkTempUser = new LinkTempUserController();

            if (!IsShortUrlInDb(link.ShortUrl))
            {
                throw new ArgumentException("Link is not in Db");
            }
            if (!IsTempUserInDb(tempUser.Key))
            {
                throw new ArgumentException("Temp user is not in Db");
            }
            linkTempUser.LinkId = link.Id;
            linkTempUser.TempUserId = tempUser.Id;
            entity.LinksTempUserController.Add(linkTempUser);
            entity.SaveChanges();
        }

        public void AddLinkAppUserControllerInDb(Link link, AppUser appUser)
        {
            LinkAppUserController linkAppUser = new LinkAppUserController();

            if (!IsShortUrlInDb(link.ShortUrl))
            {
                throw new ArgumentException("Link is not in Db");
            }
            if (!IsEmailInDb(appUser.Email))
            {
                throw new ArgumentException("App user is not in Db");
            }
            linkAppUser.LinkId = link.Id;
            linkAppUser.AppUserId = appUser.Id;
            entity.LinksAppUserController.Add(linkAppUser);
            entity.SaveChanges();
        }


        public bool IsLinkTempUserControllerInDb(int id)
        {
            if (entity.LinksTempUserController.Where(x => x.Id == id).FirstOrDefault() != null)
            {
                return true;
            }
            return false;
        }


        public LinkAppUserController GetLinkAppUserControllerFromDb(Link link)
        {
            if (!IsShortUrlInDb(link.ShortUrl))
            {
                throw new ArgumentException("No such link in Db");
            }
            return entity.LinksAppUserController.Where(x => x.LinkId == link.Id).FirstOrDefault();
        }

        public LinkTempUserController GetLinkTempUserControllerFromDb(Link link)
        {
            if (!IsShortUrlInDb(link.ShortUrl))
            {
                throw new ArgumentException("No such link in Db");
            }
            return entity.LinksTempUserController.Where(x => x.LinkId == link.Id).FirstOrDefault();
        }

        public LinkTempUserController GetLinkTempUserControllerFromDb(int id)
        {
            if (!IsLinkTempUserControllerInDb(id))
            {
                throw new ArgumentException("No such controller in Db");
            }
            return entity.LinksTempUserController.Where(x => x.Id == id).FirstOrDefault();
        }

        public List<LinkTempUserController> GetLinkTempUserControllersFromDb(string userKey)
        {
            if (!IsTempUserInDb(userKey))
            {
                throw new ArgumentException("No such user in Db");
            }
            int tempUserId = GetTempUserFromDb(userKey).Id;

            return entity.LinksTempUserController.Where(x => x.TempUserId == tempUserId).ToList();
        }

        public List<LinkTempUserController> GetAllLinkTempUserControllerFromDb()
        {
            return entity.LinksTempUserController.ToList();
        }


        public void DeleteLinkControllerFromDb(string shortUrl)
        {
            if (!IsShortUrlInDb(shortUrl))
            {
                throw new ArgumentException("No Url in Db.");
            }
            Link link = GetLinkFromDb(shortUrl);
            LinkAppUserController linkAppUserController = GetLinkAppUserControllerFromDb(link);
            if (linkAppUserController != null)
            {
                entity.LinksAppUserController.Remove(linkAppUserController);
                entity.SaveChanges();
            }
            LinkTempUserController linkTempUser = GetLinkTempUserControllerFromDb(link);
            if (linkTempUser != null)
            {
                entity.LinksTempUserController.Remove(linkTempUser);
                entity.SaveChanges();
            }
        }
        
        public void DeleteLinkTempUserControllerFromDb(int id)
        {
            LinkTempUserController linkTempUser = GetLinkTempUserControllerFromDb(id);
            if (linkTempUser != null)
            {
                entity.LinksTempUserController.Remove(linkTempUser);
                entity.SaveChanges();
            }
        }


        public void MoveTempLinksToUser(string tempUserKey, string appUserEmail)
        {
            if (!IsTempUserInDb(tempUserKey))
            {
                return;
            }
            if (!IsEmailInDb(appUserEmail))
            {
                throw new ArgumentException("No such Email in Db");
            }
            int appUserId = GetAppUserFromDb(appUserEmail).Id;
            int tempUserId = GetTempUserFromDb(tempUserKey).Id;

            List<LinkTempUserController> list = GetLinkTempUserControllersFromDb(tempUserKey);
            LinkAppUserController linkAppUser = new LinkAppUserController();
            foreach (var item in list)
            {
                linkAppUser.AppUserId = appUserId;
                linkAppUser.LinkId = item.LinkId;
                entity.LinksAppUserController.Add(linkAppUser);
                entity.LinksTempUserController.Remove(item);
                entity.SaveChanges();
            }
        }
    }
}
