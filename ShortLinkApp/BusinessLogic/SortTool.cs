﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessObject;

namespace BusinessLogic
{
    public class SortTool
    {
        public string SortBy { get; set; }
        public bool? ascendingOrder { get; set; }

        public void SortLinkList(List<Link> list, ISortMethod sortBy, bool? ascend)
        {
            sortBy.Sort(list);
            if (ascend != null && !(bool)ascend)
            {
                list.Reverse();
            }
        }
    }
}
