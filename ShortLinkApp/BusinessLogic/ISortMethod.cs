﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessObject;

namespace BusinessLogic
{
    public interface ISortMethod
    {
        void Sort(List<Link> list);
    }

    public class SortByClickCount : ISortMethod
    {
        public static readonly string CLICK_COUNT = "clickCount";

        public void Sort(List<Link> list)
        {
            list.Sort((x, y) => y.ClickCount - x.ClickCount);
        }
    }

    public class SortByOriginalUrl : ISortMethod
    {
        public static readonly string ORIGINAL_URL = "originalUrl";

        public void Sort(List<Link> list)
        {
            list.Sort((x, y) => x.OriginalUrl.CompareTo(y.OriginalUrl));
        }
    }

    public class SortByShortUrl : ISortMethod
    {
        public static readonly string SHORT_URL = "shortURL";

        public void Sort(List<Link> list)
        {
            list.Sort((x, y) => x.ShortUrl.CompareTo(y.ShortUrl));
        }
    }
}
