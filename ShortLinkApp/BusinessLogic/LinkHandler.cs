﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessObject;

namespace BusinessLogic
{
    public class LinkHandler
    {
        private Object lockThis = new Object();
        private IDatabase db;

        public LinkHandler(IDatabase database)
        {
            db = database;
        }

        public string GetUrl(string shortUrl)
        {
            if (!db.IsShortUrlInDb(shortUrl))
            {
                throw new ArgumentException();
            }
            db.UpdateLinkInDb(shortUrl);
            return db.GetLinkFromDb(shortUrl).HttpUrl;
        }

        public void AddLinkToTempUser(Link link, string userKey)
        {
            TempUser tempUser = new TempUser();
            if (!db.IsTempUserInDb(userKey))
            {
                tempUser.Key = userKey;
                db.AddTempUserInDb(tempUser);
            }
            AddLink(link);
            link = db.GetLinkFromDb(link.ShortUrl);
            tempUser = db.GetTempUserFromDb(userKey);
            db.AddLinkTempUserControllerInDb(link, tempUser);
        }

        public void AddLinkToAppUser(Link link, string userEmail)
        {
            if (!db.IsEmailInDb(userEmail))
            {
                throw new ArgumentException("Email not found in Db");
            }
            AddLink(link);
            link = db.GetLinkFromDb(link.ShortUrl);
            AppUser appUser = db.GetAppUserFromDb(userEmail);
            db.AddLinkAppUserControllerInDb(link, appUser);
        }

        private void AddLink(Link link)
        {
            Application app = new Application();
            lock (lockThis)
            {
                link.HttpUrl = AttachHttpIfNeed(link.OriginalUrl);
                do
                {
                    link.ShortUrl = app.GetRandomString(7);
                } while (db.IsShortUrlInDb(link.ShortUrl));
                link.ClickCount = 0;
                link.Created = DateTime.Now;
                link.LastVisit = link.Created;
                db.AddLinkInDb(link);
            }
        }
        
        public List<Link> GetTempUserLinks(string tempUserKey)
        {
            TempUser tempUser = new TempUser();
            if (!db.IsTempUserInDb(tempUserKey))
            {
                tempUser.Key = tempUserKey;
                tempUser.Created = DateTime.Now;
                db.AddTempUserInDb(tempUser);
            }
            tempUser = db.GetTempUserFromDb(tempUserKey);
            return db.GetTempUserLinksFromDb(tempUser.Key);
        }

        public List<Link> GetAppUserLinks(string appUserEmail)
        {
            if (!db.IsEmailInDb(appUserEmail))
            {
                throw new ArgumentException("App user is not in Db");
            }
            return db.GetAppUserLinksFromDb(appUserEmail);
        }

        public void HideLink(string shortUrl)
        {
            if (!db.IsShortUrlInDb(shortUrl))
            {
                throw new ArgumentException("No url in Db");
            }
            db.DeleteLinkControllerFromDb(shortUrl);
        }

        private string AttachHttpIfNeed(string url)
        {
            if (url == null)
            {
                return null;
            }
            if (url.StartsWith("http://") || url.StartsWith("https://"))
            {
                return url;
            }
            return "http://" + url;
        }
    }
}
