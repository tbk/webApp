﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessObject;

namespace BusinessLogic
{
    public class TempUserHandler
    {
        public IDatabase db;

        public TempUserHandler(IDatabase database)
        {
            db = database;
        }

        public void SetTempUser(string tempUserKey)
        {
            if (db.IsTempUserInDb(tempUserKey))
            {
                throw new ArgumentException("UserKey already in db");
            }
            TempUser tempUser = new TempUser();
            tempUser.Key = tempUserKey;
            tempUser.Created = DateTime.Now;
            db.AddTempUserInDb(tempUser);
        }
    }
}
