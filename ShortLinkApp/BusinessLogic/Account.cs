﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessObject;
using System.Security.Cryptography;

namespace BusinessLogic
{
    public class Account
    {
        private Object lockThis = new Object();
        private IDatabase db;

        public Account(IDatabase database)
        {
            db = database;
        }

        public void Register(AppUser user)
        {
            lock (lockThis)
            {
                if (db.IsEmailInDb(user.Email))
                {
                    throw new ArgumentException("Email already in use");
                }
                if (!db.IsAppUserStatusInDb(AppUserStatus.REGULAR))
                {
                    AppUserStatus userStatus = new AppUserStatus();
                    userStatus.Status = AppUserStatus.REGULAR;
                    db.AddAppUserStatusInDb(userStatus);
                }
                user.Password = Encrypt(user.Password);
                user.ConfirmPassword = user.Password;
                user.Created = DateTime.Now;
                user.AppUserStatusId = db.GetAppUserStatusFromDb(AppUserStatus.REGULAR).Id;
                db.AddAppUserInDb(user);
            }
        }

        public void Login(AppUser appUser, string tempUserKey)
        {
            if (!db.IsEmailInDb(appUser.Email))
            {
                throw new ArgumentException("Incorect Email");
            }
            appUser = db.GetAppUserFromDb(appUser.Email, Encrypt(appUser.Password));
            if (appUser == null)
            {
                throw new ArgumentException("Incorect Password");
            }
            db.MoveTempLinksToUser(tempUserKey, appUser.Email);
        }

        private string Encrypt(string val)
        {
            if (val == null)
            {
                return null;
            }
            var sha1 = new SHA1CryptoServiceProvider();
            UTF8Encoding utf8 = new UTF8Encoding();
            byte[] data = sha1.ComputeHash(utf8.GetBytes(val));
            return Convert.ToBase64String(data);
        }
    }
}
