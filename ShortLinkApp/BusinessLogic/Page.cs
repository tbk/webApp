﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessObject;

namespace BusinessLogic
{
    public class Page
    {
        public static readonly int ELEMENTS_PER_PAGE = 10;

        public int? Current { get; set; }
        public int Total { get; set; }

        public Page SetPageInfo(int? page, List<Link> list)
        {
            Total = (list.Count - 1) / ELEMENTS_PER_PAGE + 1;
            if (page != null && page < 1)
            {
                Current = 1;
            }
            else if (page != null && page > Total)
            {
                Current = Total;
            }
            else
            {
                Current = page;
            }
            return this;
        }
        
        public List<Link> GetPage(Page page, List<Link> list)
        {
            List<Link> newList = new List<Link>();

            if (page.Total < 1)
            {
                page = SetPageInfo(page.Current, list);
            }
            int current = page.Current == null ? 1 : (int)page.Current;
            for (int i = ELEMENTS_PER_PAGE * (current - 1); i < list.Count && newList.Count < ELEMENTS_PER_PAGE; i++)
            {
                newList.Add(list[i]);
            }
            return newList;
        }
    }
}
