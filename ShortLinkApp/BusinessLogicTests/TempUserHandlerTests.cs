﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessObject;
using Microsoft.Practices.Unity;
using DataAccess;
using DependencyInjection;

namespace BusinessLogic.Tests
{
    [TestClass()]
    public class TempUserHandlerTests
    {
        private DIContainer container;

        public TempUserHandlerTests()
        {
            container = new DIContainer();
            container.Config();
            container.RegisterType<IDatabase, MockDatabase>();
        }

        [TestInitialize]
        public void TestInitialize()
        {
            MockDatabase.AddAppUserInDbThrow = false;
            MockDatabase.IsAppUserInDbReturn = false;
            MockDatabase.IsEmailInDbReturn = false;
            MockDatabase.GetAppUserFromDbReturn = null;

            MockDatabase.AddTempUserInDbThrow = false;
            MockDatabase.IsTempUserInDbReturn = false;
            MockDatabase.GetTempUserFromDbReturn = null;
            MockDatabase.GetAllTempUsersFromDbReturn = null;
            MockDatabase.DeleteTempUserFromDbThrow = false;

            MockDatabase.AddAppUserStatusInDbThrow = false;
            MockDatabase.IsAppUserStatusInDbReturn = false;
            MockDatabase.GetAppUserStatusFromDbReturn = null;

            MockDatabase.AddLinkInDbThrow = false;
            MockDatabase.IsShortUrlInDbReturn = false;
            MockDatabase.UpdateLinkInDbThrow = false;
            MockDatabase.GetLinkFromDbReturn = null;
            MockDatabase.GetAppUserLinksFromDbReturn = null;
            MockDatabase.GetTempUserLinksFromDbReturn = null;

            MockDatabase.AddLinkTempUserControllerInDbThrow = false;
            MockDatabase.AddLinkAppUserControllerInDbThrow = false;
            MockDatabase.IsLinkTempUserControllerInDbReturn = false;
            MockDatabase.GetLinkAppUserControllerFromDbReturn = null;
            MockDatabase.GetLinkTempUserControllerFromDbReturn = null;
            MockDatabase.GetLinkTempUserControllersFromDbReturn = null;
            MockDatabase.GetAllLinkTempUserControllerFromDbReturn = null;
            MockDatabase.DeleteLinkControllerFromDbThrow = false;
            MockDatabase.DeleteLinkTempUserControllerFromDbThrow = false;
            MockDatabase.MoveTempLinksToUserThrow = false;
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        public void SetTempUserTest_CheckTempUser()
        {
            MockDatabase.IsTempUserInDbReturn = true;

            TempUserHandler tempUser = container.Resolve<TempUserHandler>();
            try
            {
                tempUser.SetTempUser("key");
            }
            catch (ArgumentException e)
            {
                Assert.AreEqual("UserKey already in db", e.Message);
                throw;
            }
        }

        [TestMethod()]
        [ExpectedException(typeof(Exception))]
        public void SetTempUserTest_CheckAddTempUser()
        {
            MockDatabase.AddTempUserInDbThrow = true;

            TempUserHandler tempUser = container.Resolve<TempUserHandler>();
            try
            {
                tempUser.SetTempUser("key");
            }
            catch (Exception e)
            {
                Assert.AreEqual("AddTempUserInDb called", e.Message);
                throw;
            }
        }
    }
}