﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Tests
{
    [TestClass()]
    public class ApplicationTests
    {
        [TestMethod()]
        public void GetRandomStringTest_CheckLength()
        {
            Application app = new Application();
            Assert.AreEqual(7, app.GetRandomString(7).Length);
        }
    }
}