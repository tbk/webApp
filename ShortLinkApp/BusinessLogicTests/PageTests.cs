﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessObject;

namespace BusinessLogic.Tests
{
    [TestClass()]
    public class PageTests
    {
        [TestMethod()]
        public void SetPageInfoTest_CheckInRangeValue()
        {
            Page pageTest = new Page();
            Page pageExpect = new Page();
            List<Link> list = new List<Link>();
            int elements;
            int? current;

            elements = 100;
            for (int i = 0; i < elements; i++)
            {
                list.Add(new Link());
            }
            current = 1;
            pageTest = pageTest.SetPageInfo(current, list);
            pageExpect.Current = 1;
            pageExpect.Total = (elements - 1) / Page.ELEMENTS_PER_PAGE + 1;

            Assert.AreEqual(pageExpect.Current, pageTest.Current);
            Assert.AreEqual(pageExpect.Total, pageTest.Total);
        }

        [TestMethod()]
        public void SetPageInfoTest_CheckNullValue()
        {
            Page pageTest = new Page();
            Page pageExpect = new Page();
            List<Link> list = new List<Link>();
            int elements;
            int? current;

            elements = 100;
            for (int i = 0; i < elements; i++)
            {
                list.Add(new Link());
            }
            current = null;
            pageTest = pageTest.SetPageInfo(current, list);
            pageExpect.Current = null;
            pageExpect.Total = (elements - 1) / Page.ELEMENTS_PER_PAGE + 1;

            Assert.AreEqual(pageExpect.Current, pageTest.Current);
            Assert.AreEqual(pageExpect.Total, pageTest.Total);
        }

        [TestMethod()]
        public void SetPageInfoTest_CheckUnderRange()
        {
            Page pageTest = new Page();
            Page pageExpect = new Page();
            List<Link> list = new List<Link>();
            int elements;
            int? current;

            elements = 100;
            for (int i = 0; i < elements; i++)
            {
                list.Add(new Link());
            }
            current = -42;
            pageTest = pageTest.SetPageInfo(current, list);
            pageExpect.Current = 1;
            pageExpect.Total = (elements - 1) / Page.ELEMENTS_PER_PAGE + 1;

            Assert.AreEqual(pageExpect.Current, pageTest.Current);
            Assert.AreEqual(pageExpect.Total, pageTest.Total);
        }

        [TestMethod()]
        public void SetPageInfoTest_CheckOverRange()
        {
            Page pageTest = new Page();
            Page pageExpect = new Page();
            List<Link> list = new List<Link>();
            int elements;
            int? current;

            elements = 100;
            for (int i = 0; i < elements; i++)
            {
                list.Add(new Link());
            }
            current = 9999999;
            pageTest = pageTest.SetPageInfo(current, list);
            pageExpect.Current = (elements - 1) / Page.ELEMENTS_PER_PAGE + 1;
            pageExpect.Total = (elements - 1) / Page.ELEMENTS_PER_PAGE + 1;

            Assert.AreEqual(pageExpect.Current, pageTest.Current);
            Assert.AreEqual(pageExpect.Total, pageTest.Total);
        }

        [TestMethod()]
        public void GetPageTest_CheckOverRange()
        {
            List<Link> list = new List<Link>();
            List<Link> listTest = new List<Link>();
            List<Link> listExpect = new List<Link>();
            Page pageInfo = new Page();
            int elements;
            int page;
            int current;

            elements = 91;
            for (int i = 0; i < elements; i++)
            {
                list.Add(new Link());
            }
            page = 654;
            pageInfo = pageInfo.SetPageInfo(page, list);
            current = pageInfo.Current == null ? 1 : (int)pageInfo.Current;
            for (int i = (current - 1) * Page.ELEMENTS_PER_PAGE; i < elements && i - (current - 1) * Page.ELEMENTS_PER_PAGE < Page.ELEMENTS_PER_PAGE; i++)
            {
                list[i].ClickCount = 42;
                listTest.Add(list[i]);
            }

            listExpect = pageInfo.GetPage(pageInfo, list);
            Assert.AreEqual(listExpect.Count, listTest.Count);
            for (int i = 0; i < listExpect.Count; i++)
            {
                Assert.AreEqual(listExpect[i], listTest[i]);
            }
        }

        [TestMethod()]
        public void GetPageTest_CheckInRange()
        {
            List<Link> list = new List<Link>();
            List<Link> listTest = new List<Link>();
            List<Link> listExpect = new List<Link>();
            Page pageInfo = new Page();
            int elements;
            int page;
            int current;

            elements = 91;
            for (int i = 0; i < elements; i++)
            {
                list.Add(new Link());
            }
            page = 1;
            pageInfo = pageInfo.SetPageInfo(page, list);
            current = pageInfo.Current == null ? 1 : (int)pageInfo.Current;
            for (int i = (current - 1) * Page.ELEMENTS_PER_PAGE; i < elements && i - (current - 1) * Page.ELEMENTS_PER_PAGE < Page.ELEMENTS_PER_PAGE; i++)
            {
                list[i].ClickCount = 42;
                listTest.Add(list[i]);
            }

            listExpect = pageInfo.GetPage(pageInfo, list);
            Assert.AreEqual(listExpect.Count, listTest.Count);
            for (int i = 0; i < listExpect.Count; i++)
            {
                Assert.AreEqual(listExpect[i], listTest[i]);
            }
        }

        [TestMethod()]
        public void GetPageTest_CheckUnderRange()
        {
            List<Link> list = new List<Link>();
            List<Link> listTest = new List<Link>();
            List<Link> listExpect = new List<Link>();
            Page pageInfo = new Page();
            int elements;
            int page;
            int current;

            elements = 91;
            for (int i = 0; i < elements; i++)
            {
                list.Add(new Link());
            }
            page = 0;
            pageInfo = pageInfo.SetPageInfo(page, list);
            current = pageInfo.Current == null ? 1 : (int)pageInfo.Current;
            for (int i = (current - 1) * Page.ELEMENTS_PER_PAGE; i < elements && i - (current - 1) * Page.ELEMENTS_PER_PAGE < Page.ELEMENTS_PER_PAGE; i++)
            {
                list[i].ClickCount = 42;
                listTest.Add(list[i]);
            }

            listExpect = pageInfo.GetPage(pageInfo, list);
            Assert.AreEqual(listExpect.Count, listTest.Count);
            for (int i = 0; i < listExpect.Count; i++)
            {
                Assert.AreEqual(listExpect[i], listTest[i]);
            }
        }
    }
}