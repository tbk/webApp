﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using BusinessObject;
using DataAccess;
using DependencyInjection;

namespace BusinessLogic.Tests
{
    [TestClass()]
    public class LinkHandlerTests
    {
        private DIContainer container;

        public LinkHandlerTests()
        {
            container = new DIContainer();
            container.Config();
            container.RegisterType<IDatabase, MockDatabase>();
        }

        [TestInitialize]
        public void TestInitialize()
        {
            MockDatabase.AddAppUserInDbThrow = false;
            MockDatabase.IsAppUserInDbReturn = false;
            MockDatabase.IsEmailInDbReturn = false;
            MockDatabase.GetAppUserFromDbReturn = null;

            MockDatabase.AddTempUserInDbThrow = false;
            MockDatabase.IsTempUserInDbReturn = false;
            MockDatabase.GetTempUserFromDbReturn = null;
            MockDatabase.GetAllTempUsersFromDbReturn = null;
            MockDatabase.DeleteTempUserFromDbThrow = false;

            MockDatabase.AddAppUserStatusInDbThrow = false;
            MockDatabase.IsAppUserStatusInDbReturn = false;
            MockDatabase.GetAppUserStatusFromDbReturn = null;

            MockDatabase.AddLinkInDbThrow = false;
            MockDatabase.IsShortUrlInDbReturn = false;
            MockDatabase.UpdateLinkInDbThrow = false;
            MockDatabase.GetLinkFromDbReturn = null;
            MockDatabase.GetAppUserLinksFromDbReturn = null;
            MockDatabase.GetTempUserLinksFromDbReturn = null;

            MockDatabase.AddLinkTempUserControllerInDbThrow = false;
            MockDatabase.AddLinkAppUserControllerInDbThrow = false;
            MockDatabase.IsLinkTempUserControllerInDbReturn = false;
            MockDatabase.GetLinkAppUserControllerFromDbReturn = null;
            MockDatabase.GetLinkTempUserControllerFromDbReturn = null;
            MockDatabase.GetLinkTempUserControllersFromDbReturn = null;
            MockDatabase.GetAllLinkTempUserControllerFromDbReturn = null;
            MockDatabase.DeleteLinkControllerFromDbThrow = false;
            MockDatabase.DeleteLinkTempUserControllerFromDbThrow = false;
            MockDatabase.MoveTempLinksToUserThrow = false;
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        public void GetUrlTest_CheckShortUrl()
        {
            LinkHandler linkHandler = container.Resolve<LinkHandler>();
            linkHandler.GetUrl("string");
        }

        [TestMethod()]
        [ExpectedException(typeof(Exception))]
        public void GetUrlTest_CheckUpdate()
        {
            MockDatabase.IsShortUrlInDbReturn = true;
            MockDatabase.UpdateLinkInDbThrow = true;

            LinkHandler linkHandler = container.Resolve<LinkHandler>();
            try
            {
                linkHandler.GetUrl("string");
            }
            catch (Exception e)
            {
                Assert.AreEqual("UpdateLinkInDb called", e.Message);
                throw;
            }
        }

        [TestMethod()]
        public void GetUrlTest_CheckReturn()
        {
            Link link = new Link() { HttpUrl = "http" };
            MockDatabase.IsShortUrlInDbReturn = true;
            MockDatabase.GetLinkFromDbReturn = link;

            LinkHandler linkHandler = container.Resolve<LinkHandler>();
            Assert.AreEqual(link.HttpUrl, linkHandler.GetUrl("string"));
        }

        [TestMethod()]
        [ExpectedException(typeof(Exception))]
        public void AddLinkToTempUserTest_CheckTempUser()
        {
            MockDatabase.AddTempUserInDbThrow = true;

            LinkHandler linkHandler = container.Resolve<LinkHandler>();
            try
            {
                linkHandler.AddLinkToTempUser(new Link(), "key");
            }
            catch (Exception e)
            {
                Assert.AreEqual("AddTempUserInDb called", e.Message);
                throw;
            }
        }

        [TestMethod()]
        [ExpectedException(typeof(Exception))]
        public void AddLinkToTempUserTest_CheckAddLink()
        {
            MockDatabase.IsTempUserInDbReturn = true;
            MockDatabase.AddLinkInDbThrow = true;

            LinkHandler linkHandler = container.Resolve<LinkHandler>();
            try
            {
                linkHandler.AddLinkToTempUser(new Link(), "key");
            }
            catch (Exception e)
            {
                Assert.AreEqual("AddLinkInDb called", e.Message);
                throw;
            }
        }

        [TestMethod()]
        [ExpectedException(typeof(Exception))]
        public void AddLinkToTempUserTest_CheckAddLinkTempUserController()
        {
            MockDatabase.IsTempUserInDbReturn = true;
            MockDatabase.GetLinkFromDbReturn = new Link();
            MockDatabase.GetTempUserFromDbReturn = new TempUser();
            MockDatabase.AddLinkTempUserControllerInDbThrow = true;

            LinkHandler linkHandler = container.Resolve<LinkHandler>();
            try
            {
                linkHandler.AddLinkToTempUser(new Link(), "key");
            }
            catch (Exception e)
            {
                Assert.AreEqual("AddLinkTempUserControllerInDb called", e.Message);
                throw;
            }
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        public void AddLinkToAppUserTest_CheckUserEmail()
        {
            LinkHandler linkHandler = container.Resolve<LinkHandler>();
            try
            {
                linkHandler.AddLinkToAppUser(new Link(), "email");
            }
            catch (ArgumentException e)
            {
                Assert.AreEqual("Email not found in Db", e.Message);
                throw;
            }
        }

        [TestMethod()]
        [ExpectedException(typeof(Exception))]
        public void AddLinkToAppUserTest_CheckAddLink()
        {
            MockDatabase.IsEmailInDbReturn = true;
            MockDatabase.AddLinkInDbThrow = true;

            LinkHandler linkHandler = container.Resolve<LinkHandler>();
            try
            {
                linkHandler.AddLinkToAppUser(new Link(), "email");
            }
            catch (Exception e)
            {
                Assert.AreEqual("AddLinkInDb called", e.Message);
                throw;
            }
        }

        [TestMethod()]
        [ExpectedException(typeof(Exception))]
        public void AddLinkToAppUserTest_CheckAddLinkAppUserController()
        {
            MockDatabase.IsEmailInDbReturn = true;
            MockDatabase.GetLinkFromDbReturn = new Link();
            MockDatabase.GetTempUserFromDbReturn = new TempUser();
            MockDatabase.AddLinkAppUserControllerInDbThrow = true;

            LinkHandler linkHandler = container.Resolve<LinkHandler>();
            try
            {
                linkHandler.AddLinkToAppUser(new Link(), "email");
            }
            catch (Exception e)
            {
                Assert.AreEqual("AddLinkAppUserControllerInDb called", e.Message);
                throw;
            }
        }

        [TestMethod()]
        [ExpectedException(typeof(Exception))]
        public void GetTempUserLinksTest_CheckTempUser()
        {
            MockDatabase.AddTempUserInDbThrow = true;

            LinkHandler linkHandler = container.Resolve<LinkHandler>();
            try
            {
                linkHandler.GetTempUserLinks("key");
            }
            catch (Exception e)
            {
                Assert.AreEqual("AddTempUserInDb called", e.Message);
                throw;
            }
        }

        [TestMethod()]
        public void GetTempUserLinksTest_CheckReturn()
        {
            List<Link> listExpect = new List<Link>();
            for (int i = 0; i < 5; i++)
            {
                listExpect.Add(new Link() { ClickCount = i });
            }
            MockDatabase.IsTempUserInDbReturn = true;
            MockDatabase.GetTempUserFromDbReturn = new TempUser();
            MockDatabase.GetTempUserLinksFromDbReturn = listExpect;

            LinkHandler linkHandler = container.Resolve<LinkHandler>();
            List<Link> listTest = linkHandler.GetTempUserLinks("key");
            Assert.AreEqual(listExpect.Count, listTest.Count);
            for (int i = 0; i < listExpect.Count; i++)
            {
                Assert.AreEqual(listExpect[i], listTest[i]);
            }
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        public void GetAppUserLinksTest_CheckUser()
        {
            LinkHandler linkHandler = container.Resolve<LinkHandler>();
            try
            {
                linkHandler.GetAppUserLinks("email");
            }
            catch (ArgumentException e)
            {
                Assert.AreEqual("App user is not in Db", e.Message);
                throw;
            }
        }

        [TestMethod()]
        public void GetAppUserLinksTest_CheckReturn()
        {
            List<Link> listExpect = new List<Link>();
            for (int i = 0; i < 5; i++)
            {
                listExpect.Add(new Link() { ClickCount = i });
            }
            MockDatabase.IsEmailInDbReturn = true;
            MockDatabase.GetAppUserLinksFromDbReturn = listExpect;

            LinkHandler linkHandler = container.Resolve<LinkHandler>();
            List<Link> listTest = linkHandler.GetAppUserLinks("email");
            Assert.AreEqual(listExpect.Count, listTest.Count);
            for (int i = 0; i < listExpect.Count; i++)
            {
                Assert.AreEqual(listExpect[i], listTest[i]);
            }
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        public void HideLinkTest_CheckShortUrl()
        {
            LinkHandler linkHandler = container.Resolve<LinkHandler>();
            try
            {
                linkHandler.HideLink("url");
            }
            catch (ArgumentException e)
            {
                Assert.AreEqual("No url in Db", e.Message);
                throw;
            }
        }

        [TestMethod()]
        [ExpectedException(typeof(Exception))]
        public void HideLinkTest_CheckDeleteLinkController()
        {
            MockDatabase.IsShortUrlInDbReturn = true;
            MockDatabase.DeleteLinkControllerFromDbThrow = true;

            LinkHandler linkHandler = container.Resolve<LinkHandler>();
            try
            {
                linkHandler.HideLink("url");
            }
            catch (Exception e)
            {
                Assert.AreEqual("DeleteLinkControllerFromDb called", e.Message);
                throw;
            }
        }
    }
}