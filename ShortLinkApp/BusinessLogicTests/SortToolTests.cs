﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessObject;

namespace BusinessLogic.Tests
{
    [TestClass()]
    public class SortToolTests
    {
        [TestMethod()]
        public void SortLinkListTest_SortByClickCountAscend()
        {
            List<Link> listTest = new List<Link>();
            List<Link> listExpect = new List<Link>();
            SortTool sortTool = new SortTool();

            listTest = InitList();
            listExpect.Add(listTest[2]);
            listExpect.Add(listTest[3]);
            listExpect.Add(listTest[0]);
            listExpect.Add(listTest[1]);

            sortTool.SortLinkList(listExpect, new SortByClickCount(), true);
            Assert.AreEqual(listExpect.Count, listTest.Count);
            for (int i = 0; i < listExpect.Count; i++)
            {
                Assert.AreEqual(listExpect[i], listTest[i]);
            }
        }

        [TestMethod()]
        public void SortLinkListTest_SortByOriginalUrlAscend()
        {
            List<Link> listTest = new List<Link>();
            List<Link> listExpect = new List<Link>();
            SortTool sortTool = new SortTool();

            listTest = InitList();
            listExpect.Add(listTest[1]);
            listExpect.Add(listTest[0]);
            listExpect.Add(listTest[3]);
            listExpect.Add(listTest[2]);

            sortTool.SortLinkList(listExpect, new SortByOriginalUrl(), true);
            Assert.AreEqual(listExpect.Count, listTest.Count);
            for (int i = 0; i < listExpect.Count; i++)
            {
                Assert.AreEqual(listExpect[i], listTest[i]);
            }
        }

        [TestMethod()]
        public void SortLinkListTest_SortByShortUrlAscend()
        {
            List<Link> listTest = new List<Link>();
            List<Link> listExpect = new List<Link>();
            SortTool sortTool = new SortTool();

            listTest = InitList();
            listExpect.Add(listTest[0]);
            listExpect.Add(listTest[3]);
            listExpect.Add(listTest[1]);
            listExpect.Add(listTest[2]);

            sortTool.SortLinkList(listExpect, new SortByShortUrl(), true);
            Assert.AreEqual(listExpect.Count, listTest.Count);
            for (int i = 0; i < listExpect.Count; i++)
            {
                Assert.AreEqual(listExpect[i], listTest[i]);
            }
        }

        [TestMethod()]
        public void SortLinkListTest_SortByClickCountDescend()
        {
            List<Link> listTest = new List<Link>();
            List<Link> listExpect = new List<Link>();
            SortTool sortTool = new SortTool();

            listTest = InitList();
            listExpect.Add(listTest[1]);
            listExpect.Add(listTest[0]);
            listExpect.Add(listTest[3]);
            listExpect.Add(listTest[2]);

            sortTool.SortLinkList(listExpect, new SortByClickCount(), false);
            Assert.AreEqual(listExpect.Count, listTest.Count);
            for (int i = 0; i < listExpect.Count; i++)
            {
                Assert.AreEqual(listExpect[i], listTest[i]);
            }
        }

        [TestMethod()]
        public void SortLinkListTest_SortByOriginalUrlDescend()
        {
            List<Link> listTest = new List<Link>();
            List<Link> listExpect = new List<Link>();
            SortTool sortTool = new SortTool();

            listTest = InitList();
            listExpect.Add(listTest[2]);
            listExpect.Add(listTest[3]);
            listExpect.Add(listTest[0]);
            listExpect.Add(listTest[1]);

            sortTool.SortLinkList(listExpect, new SortByOriginalUrl(), false);
            Assert.AreEqual(listExpect.Count, listTest.Count);
            for (int i = 0; i < listExpect.Count; i++)
            {
                Assert.AreEqual(listExpect[i], listTest[i]);
            }
        }

        [TestMethod()]
        public void SortLinkListTest_SortByShortUrlDescend()
        {
            List<Link> listTest = new List<Link>();
            List<Link> listExpect = new List<Link>();
            SortTool sortTool = new SortTool();

            listTest = InitList();
            listExpect.Add(listTest[2]);
            listExpect.Add(listTest[1]);
            listExpect.Add(listTest[3]);
            listExpect.Add(listTest[0]);

            sortTool.SortLinkList(listExpect, new SortByShortUrl(), false);
            Assert.AreEqual(listExpect.Count, listTest.Count);
            for (int i = 0; i < listExpect.Count; i++)
            {
                Assert.AreEqual(listExpect[i], listTest[i]);
            }
        }

        [TestMethod()]
        public void SortLinkListTest_SortByClickCountDefault()
        {
            List<Link> listTest = new List<Link>();
            List<Link> listExpect = new List<Link>();
            SortTool sortTool = new SortTool();

            listTest = InitList();
            listExpect.Add(listTest[2]);
            listExpect.Add(listTest[3]);
            listExpect.Add(listTest[0]);
            listExpect.Add(listTest[1]);

            sortTool.SortLinkList(listExpect, new SortByClickCount(), null);
            Assert.AreEqual(listExpect.Count, listTest.Count);
            for (int i = 0; i < listExpect.Count; i++)
            {
                Assert.AreEqual(listExpect[i], listTest[i]);
            }
        }

        [TestMethod()]
        public void SortLinkListTest_SortByOriginalUrlDefault()
        {
            List<Link> listTest = new List<Link>();
            List<Link> listExpect = new List<Link>();
            SortTool sortTool = new SortTool();

            listTest = InitList();
            listExpect.Add(listTest[1]);
            listExpect.Add(listTest[0]);
            listExpect.Add(listTest[3]);
            listExpect.Add(listTest[2]);

            sortTool.SortLinkList(listExpect, new SortByOriginalUrl(), null);
            Assert.AreEqual(listExpect.Count, listTest.Count);
            for (int i = 0; i < listExpect.Count; i++)
            {
                Assert.AreEqual(listExpect[i], listTest[i]);
            }
        }

        [TestMethod()]
        public void SortLinkListTest_SortByShortUrlDefault()
        {
            List<Link> listTest = new List<Link>();
            List<Link> listExpect = new List<Link>();
            SortTool sortTool = new SortTool();

            listTest = InitList();
            listExpect.Add(listTest[0]);
            listExpect.Add(listTest[3]);
            listExpect.Add(listTest[1]);
            listExpect.Add(listTest[2]);

            sortTool.SortLinkList(listExpect, new SortByShortUrl(), null);
            Assert.AreEqual(listExpect.Count, listTest.Count);
            for (int i = 0; i < listExpect.Count; i++)
            {
                Assert.AreEqual(listExpect[i], listTest[i]);
            }
        }

        private List<Link> InitList()
        {
            List<Link> list = new List<Link>();
            Link link = new Link();

            link.ClickCount = 6;
            link.OriginalUrl = "g";
            link.ShortUrl = "b";
            list.Add(link);

            link.ClickCount = 10;
            link.OriginalUrl = "a";
            link.ShortUrl = "i";
            list.Add(link);

            link.ClickCount = 4;
            link.OriginalUrl = "z";
            link.ShortUrl = "l";
            list.Add(link);

            link.ClickCount = 5;
            link.OriginalUrl = "r";
            link.ShortUrl = "e";
            list.Add(link);
            return list;
        }

    }
}