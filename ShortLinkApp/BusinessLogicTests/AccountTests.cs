﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using BusinessObject;
using DataAccess;
using DependencyInjection;

namespace BusinessLogic.Tests
{
    [TestClass()]
    public class AccountTests
    {
        private DIContainer container;

        public AccountTests()
        {
            container = new DIContainer();
            container.Config();
            container.RegisterType<IDatabase, MockDatabase>();
        }

        [TestInitialize]
        public void TestInitialize()
        {
            MockDatabase.AddAppUserInDbThrow = false;
            MockDatabase.IsAppUserInDbReturn = false;
            MockDatabase.IsEmailInDbReturn = false;
            MockDatabase.GetAppUserFromDbReturn = null;

            MockDatabase.AddTempUserInDbThrow = false;
            MockDatabase.IsTempUserInDbReturn = false;
            MockDatabase.GetTempUserFromDbReturn = null;
            MockDatabase.GetAllTempUsersFromDbReturn = null;
            MockDatabase.DeleteTempUserFromDbThrow = false;

            MockDatabase.AddAppUserStatusInDbThrow = false;
            MockDatabase.IsAppUserStatusInDbReturn = false;
            MockDatabase.GetAppUserStatusFromDbReturn = null;

            MockDatabase.AddLinkInDbThrow = false;
            MockDatabase.IsShortUrlInDbReturn = false;
            MockDatabase.UpdateLinkInDbThrow = false;
            MockDatabase.GetLinkFromDbReturn = null;
            MockDatabase.GetAppUserLinksFromDbReturn = null;
            MockDatabase.GetTempUserLinksFromDbReturn = null;

            MockDatabase.AddLinkTempUserControllerInDbThrow = false;
            MockDatabase.AddLinkAppUserControllerInDbThrow = false;
            MockDatabase.IsLinkTempUserControllerInDbReturn = false;
            MockDatabase.GetLinkAppUserControllerFromDbReturn = null;
            MockDatabase.GetLinkTempUserControllerFromDbReturn = null;
            MockDatabase.GetLinkTempUserControllersFromDbReturn = null;
            MockDatabase.GetAllLinkTempUserControllerFromDbReturn = null;
            MockDatabase.DeleteLinkControllerFromDbThrow = false;
            MockDatabase.DeleteLinkTempUserControllerFromDbThrow = false;
            MockDatabase.MoveTempLinksToUserThrow = false;
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        public void RegisterTest_CheckEmail()
        {
            MockDatabase.IsEmailInDbReturn = true;

            Account acc = container.Resolve<Account>();
            try
            {
                acc.Register(new AppUser());
            }
            catch (ArgumentException e)
            {
                Assert.AreEqual("Email already in use", e.Message);
                throw;
            }
        }

        [TestMethod()]
        [ExpectedException(typeof(Exception))]
        public void RegisterTest_CheckAppUserStatus()
        {
            MockDatabase.AddAppUserStatusInDbThrow = true;

            Account acc = container.Resolve<Account>();
            try
            {
                acc.Register(new AppUser());
            }
            catch (Exception e)
            {
                Assert.AreEqual("AddAppUserStatusInDb called", e.Message);
                throw;
            }
        }

        [TestMethod()]
        [ExpectedException(typeof(Exception))]
        public void RegisterTest_AddNewUser()
        {
            MockDatabase.IsAppUserStatusInDbReturn = true;
            MockDatabase.GetAppUserStatusFromDbReturn = new AppUserStatus();
            MockDatabase.AddAppUserInDbThrow = true;

            Account acc = container.Resolve<Account>();
            try
            {
                acc.Register(new AppUser());
            }
            catch (Exception e)
            {
                Assert.AreEqual("AddAppUserInDb called", e.Message);
                throw;
            }
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        public void LoginTest_CheckEmail()
        {
            Account acc = container.Resolve<Account>();
            try
            {
                acc.Login(new AppUser(), "key");
            }
            catch (ArgumentException e)
            {
                Assert.AreEqual("Incorect Email", e.Message);
                throw;
            }
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        public void LoginTest_CheckPassword()
        {
            MockDatabase.IsEmailInDbReturn = true;

            Account acc = container.Resolve<Account>();
            try
            {
                acc.Login(new AppUser(), "key");
            }
            catch (ArgumentException e)
            {
                Assert.AreEqual("Incorect Password", e.Message);
                throw;
            }
        }

        [TestMethod()]
        [ExpectedException(typeof(Exception))]
        public void LoginTest_CheckMoveLinks()
        {
            MockDatabase.IsEmailInDbReturn = true;
            MockDatabase.GetAppUserFromDbReturn = new AppUser();
            MockDatabase.MoveTempLinksToUserThrow = true;

            Account acc = container.Resolve<Account>();
            try
            {
                acc.Login(new AppUser(), "key");
            }
            catch (Exception e)
            {
                Assert.AreEqual("MoveTempLinksToUser called", e.Message);
                throw;
            }
        }
    }
}