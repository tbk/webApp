﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using BusinessObject;

namespace DatabaseGarbageCollector
{
    public class GarbageCollector
    {
        private IDatabase db;

        public GarbageCollector(IDatabase database)
        {
            db = database;
        }

        public void Start()
        {
            while (true)
            {
                ClearTempUsers();
                ClearLinkTempUserController();
                Thread.Sleep(86400);//1 day
            }
        }

        public void ClearTempUsers()
        {
            List<TempUser> list = db.GetAllTempUsersFromDb();

            foreach (var item in list)
            {
                if (item.Created.AddDays(2) < DateTime.Now)
                {
                    db.DeleteTempUserFromDb(item.Key);
                }
            }
        }

        public void ClearLinkTempUserController()
        {
            List<LinkTempUserController> list = db.GetAllLinkTempUserControllerFromDb();

            foreach (var item in list)
            {
                if (!db.IsTempUserInDb(item.TempUserId))
                {
                    db.DeleteLinkTempUserControllerFromDb(item.Id);
                }
            }
        }
    }
}
