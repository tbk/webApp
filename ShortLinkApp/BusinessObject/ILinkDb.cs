﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject
{
    public interface ILinkDb
    {
        void AddLinkInDb(Link link);

        bool IsShortUrlInDb(string shortUrl);
        bool IsShortUrlInDb(int id);

        void UpdateLinkInDb(string shortUrl);

        Link GetLinkFromDb(string shortUrl);
        List<Link> GetAppUserLinksFromDb(string userEmail);
        List<Link> GetTempUserLinksFromDb(string userKey);
    }
}
