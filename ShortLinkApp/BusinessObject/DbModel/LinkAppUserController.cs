﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject
{
    public class LinkAppUserController
    {
        public int Id { get; set; }

        public int LinkId { get; set; }

        public int AppUserId { get; set; }

        public virtual Link Link { get; set; }
        public virtual AppUser AppUser { get; set; }

    }
}
