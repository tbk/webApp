﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject
{
    public class TempUser
    {
        public int Id { get; set; }

        public string Key { get; set; }

        public DateTime Created { get; set; }

        public virtual List<LinkTempUserController> LinksTempUsers { get; set; }
    }
}
