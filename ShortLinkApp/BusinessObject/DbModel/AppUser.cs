﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace BusinessObject
{
    public class AppUser
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Email is required.")]
        [EmailAddress(ErrorMessage = "Please enter a vaild email.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Password is Required.")]
        [MinLength(4, ErrorMessage = "Password should have at least 4 characters")]
        [MaxLength(50, ErrorMessage = "Password should have no more than 50 characters")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Confirm Password is Required.")]
        [Compare("Password", ErrorMessage = "Confirm password.")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }

        public DateTime Created { get; set; }

        public int AppUserStatusId { get; set; }

        public virtual AppUserStatus AppUserStatus { get; set; }

        public virtual List<LinkAppUserController> LinksAppUsers { get; set; }
    }
}
