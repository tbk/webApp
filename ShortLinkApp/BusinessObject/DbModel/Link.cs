﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace BusinessObject
{
    public class Link
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Url is Required.")]
        [RegularExpression(@"^[^\s]*$", ErrorMessage = "No spaces allowed")]
        public string OriginalUrl { get; set; }

        public string HttpUrl { get; set; }

        public string ShortUrl { get; set; }

        public int ClickCount { get; set; }

        public DateTime Created { get; set; }

        public DateTime LastVisit { get; set; }
    }
}
