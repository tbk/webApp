﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace BusinessObject
{
    public class AppDbEntities : DbContext
    {
        public virtual DbSet<AppUser> AppUsers { get; set; }
        public virtual DbSet<AppUserStatus> AppUserStatuses { get; set; }
        public virtual DbSet<Link> Links { get; set; }
        public virtual DbSet<TempUser> TempUsers { get; set; }
        public virtual DbSet<LinkTempUserController> LinksTempUserController { get; set; }
        public virtual DbSet<LinkAppUserController> LinksAppUserController { get; set; }
    }
}
