﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject
{
    public class AppUserStatus
    {
        public static readonly string ADMIN = "admin";
        public static readonly string REGULAR = "user";

        public int Id { get; set; }

        public string Status { get; set; }

        public virtual List<AppUser> AppUsers { get; set; }
    }
}
