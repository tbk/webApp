﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject
{
    public interface IAppUserStatusDb
    {
        void AddAppUserStatusInDb(AppUserStatus userStatus);

        bool IsAppUserStatusInDb(string status);
        bool IsAppUserStatusInDb(int id);

        AppUserStatus GetAppUserStatusFromDb(string status);
    }
}
