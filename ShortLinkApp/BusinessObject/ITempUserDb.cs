﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject
{
    public interface ITempUserDb
    {
        void AddTempUserInDb(TempUser tempUser);

        bool IsTempUserInDb(string tempUserKey);
        bool IsTempUserInDb(int id);

        TempUser GetTempUserFromDb(string userKey);
        List<TempUser> GetAllTempUsersFromDb();

        void DeleteTempUserFromDb(string userKey);
    }
}
