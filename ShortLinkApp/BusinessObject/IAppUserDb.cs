﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject
{
    public interface IAppUserDb
    {
        void AddAppUserInDb(AppUser appUser);

        bool IsAppUserInDb(int id);
        bool IsEmailInDb(string email);

        AppUser GetAppUserFromDb(string email);
        AppUser GetAppUserFromDb(string email, string password);
    }
}
