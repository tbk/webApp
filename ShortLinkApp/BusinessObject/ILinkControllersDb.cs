﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject
{
    public interface ILinkControllersDb
    {
        void AddLinkTempUserControllerInDb(Link link, TempUser tempUser);
        void AddLinkAppUserControllerInDb(Link link, AppUser appUser);

        bool IsLinkTempUserControllerInDb(int id);

        LinkAppUserController GetLinkAppUserControllerFromDb(Link link);
        LinkTempUserController GetLinkTempUserControllerFromDb(Link link);
        LinkTempUserController GetLinkTempUserControllerFromDb(int id);
        List<LinkTempUserController> GetLinkTempUserControllersFromDb(string userKey);
        List<LinkTempUserController> GetAllLinkTempUserControllerFromDb();

        void DeleteLinkControllerFromDb(string shortUrl);
        void DeleteLinkTempUserControllerFromDb(int id);

        void MoveTempLinksToUser(string tempUserKey, string appUserEmail);
    }
}
