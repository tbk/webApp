﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DatabaseGarbageCollector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using BusinessObject;
using DataAccess;
using DependencyInjection;

namespace DatabaseGarbageCollector.Tests
{
    [TestClass()]
    public class GarbageCollectorTests
    {
        private DIContainer container;

        public GarbageCollectorTests()
        {
            container = new DIContainer();
            container.Config();
            container.RegisterType<IDatabase, MockDatabase>();
        }

        [TestInitialize]
        public void TestInitialize()
        {
            MockDatabase.AddAppUserInDbThrow = false;
            MockDatabase.IsAppUserInDbReturn = false;
            MockDatabase.IsEmailInDbReturn = false;
            MockDatabase.GetAppUserFromDbReturn = null;

            MockDatabase.AddTempUserInDbThrow = false;
            MockDatabase.IsTempUserInDbReturn = false;
            MockDatabase.GetTempUserFromDbReturn = null;
            MockDatabase.GetAllTempUsersFromDbReturn = null;
            MockDatabase.DeleteTempUserFromDbThrow = false;

            MockDatabase.AddAppUserStatusInDbThrow = false;
            MockDatabase.IsAppUserStatusInDbReturn = false;
            MockDatabase.GetAppUserStatusFromDbReturn = null;

            MockDatabase.AddLinkInDbThrow = false;
            MockDatabase.IsShortUrlInDbReturn = false;
            MockDatabase.UpdateLinkInDbThrow = false;
            MockDatabase.GetLinkFromDbReturn = null;
            MockDatabase.GetAppUserLinksFromDbReturn = null;
            MockDatabase.GetTempUserLinksFromDbReturn = null;

            MockDatabase.AddLinkTempUserControllerInDbThrow = false;
            MockDatabase.AddLinkAppUserControllerInDbThrow = false;
            MockDatabase.IsLinkTempUserControllerInDbReturn = false;
            MockDatabase.GetLinkAppUserControllerFromDbReturn = null;
            MockDatabase.GetLinkTempUserControllerFromDbReturn = null;
            MockDatabase.GetLinkTempUserControllersFromDbReturn = null;
            MockDatabase.GetAllLinkTempUserControllerFromDbReturn = null;
            MockDatabase.DeleteLinkControllerFromDbThrow = false;
            MockDatabase.DeleteLinkTempUserControllerFromDbThrow = false;
            MockDatabase.MoveTempLinksToUserThrow = false;
        }

        [TestMethod()]
        [ExpectedException(typeof(Exception))]
        public void ClearTempUsersTest_CheckDelete()
        {
            List<TempUser> list = new List<TempUser>();

            list.Add(new TempUser() { Created = DateTime.Now.AddHours(-49) });
            MockDatabase.GetAllTempUsersFromDbReturn = list;
            MockDatabase.DeleteTempUserFromDbThrow = true;

            GarbageCollector gc = container.Resolve<GarbageCollector>();
            try
            {
                gc.ClearTempUsers();
            }
            catch (Exception e)
            {
                Assert.AreEqual("DeleteTempUserFromDb called", e.Message);
                throw;
            }
        }

        [TestMethod()]
        public void ClearTempUsersTest_CheckNoAction()
        {
            List<TempUser> list = new List<TempUser>();

            list.Add(new TempUser() { Created = DateTime.Now.AddHours(-47) });
            MockDatabase.GetAllTempUsersFromDbReturn = list;
            MockDatabase.DeleteTempUserFromDbThrow = true;

            GarbageCollector gc = container.Resolve<GarbageCollector>();
            gc.ClearTempUsers();
        }

        [TestMethod()]
        [ExpectedException(typeof(Exception))]
        public void ClearLinkTempUserControllerTest_CheckDeleteLinkTempUserController()
        {
            List<LinkTempUserController> list = new List<LinkTempUserController>();

            list.Add(new LinkTempUserController());
            MockDatabase.GetAllLinkTempUserControllerFromDbReturn = list;
            MockDatabase.DeleteLinkTempUserControllerFromDbThrow = true;

            GarbageCollector gc = container.Resolve<GarbageCollector>();
            try
            {
                gc.ClearLinkTempUserController();
            }
            catch (Exception e)
            {
                Assert.AreEqual("DeleteLinkTempUserControllerFromDb called", e.Message);
                throw;
            }
        }

        [TestMethod()]
        public void ClearLinkTempUserControllerTest_CheckNoAction()
        {
            List<LinkTempUserController> list = new List<LinkTempUserController>();

            list.Add(new LinkTempUserController());
            MockDatabase.GetAllLinkTempUserControllerFromDbReturn = list;
            MockDatabase.IsTempUserInDbReturn = true;
            MockDatabase.DeleteLinkTempUserControllerFromDbThrow = true;

            GarbageCollector gc = container.Resolve<GarbageCollector>();
            gc.ClearLinkTempUserController();
        }
    }
}