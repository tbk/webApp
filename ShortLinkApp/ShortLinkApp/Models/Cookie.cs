﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using BusinessLogic;
using Microsoft.Practices.Unity;
using DependencyInjection;

namespace ShortLinkApp
{
    public class Cookie
    {
        public static readonly string COOKIE_NAME = "appCookie";
        private Object lockThis = new Object();
        private DIContainer container;

        public Cookie()
        {
            container = new DIContainer();
            container.Config();
        }
        
        public void SetCookie()
        {
            TempUserHandler tempUser = container.Resolve<TempUserHandler>();
            if (IsCookieSet(COOKIE_NAME))
            {
                try
                {
                    tempUser.SetTempUser(GetCookieVal(COOKIE_NAME));
                }
                catch (ArgumentException)
                {
                    return;
                }
                DestroyCookie(COOKIE_NAME);
            }
            Application app = new Application();
            lock (lockThis)
            {
                if (!IsCookieSet(COOKIE_NAME))
                {
                    HttpCookie appCookie = new HttpCookie(COOKIE_NAME);
                    while (true)
                    {
                        appCookie.Value = app.GetRandomString(8);
                        appCookie.Expires = DateTime.Now.AddDays(1);
                        try
                        {
                            tempUser.SetTempUser(appCookie.Value);
                        }
                        catch (ArgumentException)
                        {
                            continue;
                        }
                        break;
                    }
                    AddCookie(appCookie);
                }
            }
        }

        public bool IsCookieSet(string cookieName)
        {
            if (HttpContext.Current.Request.Cookies[cookieName] == null)
            {
                return false;
            }
            return true;
        }

        public void DestroyCookie(string cookieName)
        {
            HttpCookie cookie = new HttpCookie(cookieName);
            cookie.Value = null;
            cookie.Expires = DateTime.Now.AddDays(-1);
            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        public string GetCookieVal(string cookieName)
        {
            return HttpContext.Current.Request.Cookies[cookieName].Value;
        }

        public void AddCookie(HttpCookie cookie)
        {
            HttpContext.Current.Response.Cookies.Add(cookie);
        }
    }
}
