﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Practices.Unity;
using BusinessObject;
using BusinessLogic;
using DependencyInjection;

namespace ShortLinkApp.Models
{

    public class HomeIndexModel
    {
        private DIContainer container;

        public Link Link { get; set; }
        public List<Link> List { get; set; }
        public Page PageInfo { get; set; }
        public SortTool Sort { get; set; }

        public HomeIndexModel()
        {
            List = new List<Link>();
            Link = new Link();
            PageInfo = new Page();
            Sort = new SortTool();
            container = new DIContainer();
            container.Config();
        }

        public HomeIndexModel SetTempUserModel(int? page, string sortBy, bool? ascendingOrder, string userKey)
        {
            LinkHandler linkHndl = container.Resolve<LinkHandler>();
            List = linkHndl.GetTempUserLinks(userKey);
            return GetModel(page, sortBy, ascendingOrder);
        }
        
        public HomeIndexModel SetAppUserModel(int? page, string sortBy, bool? ascendingOrder, string userEmail)
        {
            LinkHandler linkHndl = container.Resolve<LinkHandler>();
            List = linkHndl.GetAppUserLinks(userEmail);
            return GetModel(page, sortBy, ascendingOrder);
        }

        private HomeIndexModel GetModel(int? page, string sortBy, bool? ascendingOrder)
        {
            if (sortBy != null)
            {
                ISortMethod sortMethod = null;
                if (sortBy.Equals(SortByOriginalUrl.ORIGINAL_URL))
                {
                    sortMethod = new SortByOriginalUrl();
                }
                else if (sortBy.Equals(SortByShortUrl.SHORT_URL))
                {
                    sortMethod = new SortByShortUrl();
                }
                else if (sortBy.Equals(SortByClickCount.CLICK_COUNT))
                {
                    sortMethod = new SortByClickCount();
                }
                if (sortMethod != null)
                {
                    Sort.SortLinkList(List, sortMethod, ascendingOrder);
                }
            }
            Sort.ascendingOrder = ascendingOrder;
            Sort.SortBy = sortBy;
            PageInfo = PageInfo.SetPageInfo(page, List);
            List = PageInfo.GetPage(PageInfo, List);
            return this;
        }
    }
}