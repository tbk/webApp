﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading;
using Microsoft.Practices.Unity;
using DependencyInjection;

namespace ShortLinkApp.App_Start
{
    public class GarbageCollector
    {
        private static DIContainer container;

        static GarbageCollector()
        {
            container = new DIContainer();
            container.Config();
        }

        public static void Start()
        {
            DatabaseGarbageCollector.GarbageCollector gc = container.Resolve<DatabaseGarbageCollector.GarbageCollector>();
            Thread thread = new Thread(gc.Start);
            thread.Start();
        }
    }
}