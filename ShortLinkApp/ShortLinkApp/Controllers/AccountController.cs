﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ShortLinkApp.Models;
using Microsoft.Practices.Unity;
using BusinessObject;
using BusinessLogic;
using DependencyInjection;

namespace ShortLinkApp.Controllers
{
    public class AccountController : Controller
    {
        private DIContainer container;

        public AccountController()
        {
            container = new DIContainer();
            container.Config();
        }
        
        public ActionResult Register()
        {
            return View();
        }
        
        [HttpPost]
        public ActionResult Register(AppUser user)
        {
            if (ModelState.IsValid)
            {
                string pass = user.Password;
                Account acc = container.Resolve<Account>();
                try
                {
                    acc.Register(user);
                }
                catch (ArgumentException e)
                {
                    ViewBag.Error = e.Message;
                    return View();
                }
                Cookie cookie = new Cookie();
                user.Password = pass;
                return Login(user);
            }
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }
        
        [HttpPost]
        public ActionResult Login(AppUser user)
        {
            Account acc = container.Resolve<Account>();
            try
            {
                Cookie cookie = new Cookie();
                acc.Login(user, cookie.GetCookieVal(Cookie.COOKIE_NAME));
            }
            catch (ArgumentException e)
            {
                ViewBag.Error = e.Message;
                return View();
            }
            Session["UserEmail"] = user.Email;
            return RedirectToAction("Index", "Home");
        }

        public ActionResult LogOut()
        {
            Session["UserEmail"] = null;
            return RedirectToAction("Index", "Home");
        }
    }
}