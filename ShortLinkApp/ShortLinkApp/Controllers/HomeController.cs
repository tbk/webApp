﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Practices.Unity;
using ShortLinkApp.Models;
using BusinessLogic;
using DependencyInjection;

namespace ShortLinkApp.Controllers
{
    public class HomeController : Controller
    {
        private DIContainer container;

        public HomeController()
        {
            container = new DIContainer();
            container.Config();
        }

        public ActionResult Index(string shortUrl, int? page, string sortBy, bool? ascendingOrder)
        {
            LinkHandler linkHndl = container.Resolve<LinkHandler>();
            if (shortUrl == null)
            {
                HomeIndexModel model = new HomeIndexModel();
                if (Session["userEmail"] != null)
                {
                    model = model.SetAppUserModel(page, sortBy, ascendingOrder, Session["userEmail"].ToString());
                }
                else
                {
                    Cookie cookie = new Cookie();
                    cookie.SetCookie();
                    model = model.SetTempUserModel(page, sortBy, ascendingOrder, cookie.GetCookieVal(Cookie.COOKIE_NAME));
                }
                return View(model);
            }
            else
            {
                try
                {
                    return Redirect(linkHndl.GetUrl(shortUrl));
                }
                catch (ArgumentException)
                {
                    return HttpNotFound();
                }
            }
        }
        
        [HttpPost]
        public ActionResult Index(HomeIndexModel model)
        {
            Cookie cookie = new Cookie();
            LinkHandler linkHndl = container.Resolve<LinkHandler>();
            if (ModelState.IsValid)
            {
                if (Session["userEmail"] != null)
                {
                    linkHndl.AddLinkToAppUser(model.Link, Session["userEmail"].ToString());
                }
                else
                {
                    cookie.SetCookie();
                    linkHndl.AddLinkToTempUser(model.Link, cookie.GetCookieVal(Cookie.COOKIE_NAME));
                }
                return RedirectToAction("Index", new { page = model.PageInfo.Current, sortBy = model.Sort.SortBy, ascendingOrder = model.Sort.ascendingOrder });
            }
            if (Session["userEmail"] != null)
            {
                model = model.SetAppUserModel(model.PageInfo.Current, model.Sort.SortBy, model.Sort.ascendingOrder, Session["userEmail"].ToString());
            }
            else
            {
                cookie.SetCookie();
                model = model.SetTempUserModel(model.PageInfo.Current, model.Sort.SortBy, model.Sort.ascendingOrder, cookie.GetCookieVal(Cookie.COOKIE_NAME));
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Hide(HomeIndexModel model)
        {
            LinkHandler linkHndl = container.Resolve<LinkHandler>();
            try
            {
                linkHndl.HideLink(model.Link.ShortUrl);
            }
            catch (ArgumentException e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            return RedirectToAction("Index", new { page = model.PageInfo.Current, sortBy = model.Sort.SortBy, ascendingOrder = model.Sort.ascendingOrder });
        }
    }
}