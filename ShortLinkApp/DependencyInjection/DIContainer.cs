﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;
using BusinessObject;

namespace DependencyInjection
{
    public class DIContainer : UnityContainer
    {
        public void Config()
        {
            this.RegisterType<IDatabase, Database>();
        }
    }
}
